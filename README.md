# _Домашнее задание №1_

1. Зарегистрироваться на GitLab;
2. Сгенерировать SSH-key;
3. Добавить ключ в GitLab;
4. Сделать публичный репозиторий;
5. Склонировать его к себе на компьютер;
6. Добавить в него как минимум один коммит (к примеру создать файл Esoft_School.txt, содержащий строчку “Привет, шаришь за хвост феи?“);
7. Запушить изменения в удалённый репозиторий, который создали ранее;
8. Проверить, что в этом репозитории появился ваш коммит;
9. Прислать нам в форму ваш никнейм на GitLab и ссылку на репозиторий, который вы создали.
10. После выполнения задания:
11. Зайдите во вкладку «Информация» данной формы
12. В поле «Ответ» приложите выполненное задание - ссылку на репозиторий
13. Нажмите на кнопку «Отправить на проверку» 